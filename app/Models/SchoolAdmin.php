<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class SchoolAdmin extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;
    protected $hidden = [
        'password',
    ];
}
