<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Teacher extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $hidden = [
        'password',
    ];

    public function SchoolTeacher()
    {
        return $this->hasOne(SchoolTeacher::class, 'teacher_id', 'id');
    }
}
