<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolTeacher extends Model
{
    public function School()
    {
        return $this->belongsTo(School::class, 'school_id', 'id');
    }
}
