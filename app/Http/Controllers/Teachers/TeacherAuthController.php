<?php

namespace App\Http\Controllers\Teachers;

use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\SchoolTeacher;
use Illuminate\Support\Facades\Validator;
use App\Responses\Response;

class TeacherAuthController extends Controller
{
    public function register(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:teachers',
            'password'  => 'required|min:6|confirmed',
            'school_id' => 'required|integer'
        ]);

        if ($rules->fails()) {
            return Response::send(422, $rules->errors());
        }

        $teacher = new Teacher();
        $teacher->email = $request->email;
        $teacher->name = $request->name;
        $teacher->password = Hash::make($request->password);
        $teacher->save();

        $schoolAdmin = new SchoolTeacher();
        $schoolAdmin->school_id = $request->school_id;
        $schoolAdmin->teacher_id = $teacher->id;
        $schoolAdmin->save();

        return Response::send(200, null, 'success');
    }

    public function login(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'email' => 'required|email',
            'password'  => 'required|string',
            'school_id' => 'required|integer'
        ]);

        if ($rules->fails()) {
            return Response::send(422, $rules->errors());
        }

        $teacher = Teacher::where('email', $request->email)
                    ->with(['SchoolTeacher'=>function ($query) use ($request) {
                        return $query->where('school_id', $request->school_id);
                    },
                    'SchoolTeacher.School'
                    ])
                    ->whereHas('SchoolTeacher.School', function ($query) use ($request) {
                        return $query->where('id', $request->school_id);
                    })
                    ->first();

        if (!$teacher || !Hash::check($request->password, $teacher->password)) {
            return Response::message('unknown_credentials');
        }

        $token = $teacher->createToken('teacher-token')->plainTextToken;

        $response = [
            'user' => $teacher,
            'token' => $token,
            'role' => 'teacher'
        ];

        return Response::send(200, $response);
    }

    public function index()
    {
        return Response::send(200, Auth::user());
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return Response::send(200, null, 'logged_out');
    }
}
