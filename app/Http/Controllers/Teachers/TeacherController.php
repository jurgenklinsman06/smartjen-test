<?php

namespace App\Http\Controllers\Teachers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Responses\Response;
use App\Services\SchoolService;

class TeacherController extends Controller
{
    public function students(Request $request, $schoolId, SchoolService $schoolService)
    {
        $students = $schoolService->students($request, $schoolId);

        return Response::send(200, $students);
    }

    public function teachers(Request $request, $schoolId, SchoolService $schoolService)
    {
        $teachers = $schoolService->teachers($request, $schoolId);

        return Response::send(200, $teachers);
    }
}
