<?php

namespace App\Http\Controllers\Students;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Responses\Response;
use Illuminate\Support\Facades\Validator;

class StudentAuthController extends Controller
{
    public function register(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:school_admins',
            'password'  => 'required|min:6|confirmed',
            'school_id' => 'required|integer'
        ]);

        if ($rules->fails()) {
            return Response::send(422, $rules->errors());
        }

        $student = new Student();
        $student->email = $request->email;
        $student->name = $request->name;
        $student->school_id = $request->school_id;
        $student->password = Hash::make($request->password);
        $student->save();

        return Response::send(200, null, 'success');
    }

    public function login(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'email' => 'required|email',
            'password'  => 'required|string',
        ]);

        if ($rules->fails()) {
            return Response::send(422, $rules->errors());
        }

        $student = Student::where('email', $request->email)->first();
        
        if (!$student || !Hash::check($request->password, $student->password)) {
            return Response::message('unknown_credentials');
        }

        $token = $student->createToken('student-token')->plainTextToken;

        $response = [
            'user' => $student,
            'token' => $token,
            'role' => 'student'
        ];

        return Response::send(200, $response);
    }

    public function index()
    {
        return Response::send(200, Auth::user());
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return Response::send(200, null, 'logged_out');
    }
}
