<?php

namespace App\Http\Controllers\Students;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Responses\Response;
use App\Services\SchoolService;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function students(Request $request, SchoolService $schoolService)
    {
        $students = $schoolService->students($request, Auth::user()->school_id);

        return Response::send(200, $students);
    }

    public function teachers(Request $request, SchoolService $schoolService)
    {
        $teachers = $schoolService->teachers($request, Auth::user()->school_id);

        return Response::send(200, $teachers);
    }
}
