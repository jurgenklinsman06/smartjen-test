<?php

namespace App\Http\Controllers\Schools;

use App\Models\SchoolAdmin;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Responses\Response;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:school_admins',
            'password'  => 'required|min:6|confirmed',
            'school_name' => 'required|string'
        ]);

        if ($rules->fails()) {
            return Response::send(422, $rules->errors());
        }

        $school = new School();
        $school->name = $request->school_name;
        $school->save();

        $schoolAdmin = new SchoolAdmin();
        $schoolAdmin->email = $request->email;
        $schoolAdmin->name = $request->name;
        $schoolAdmin->school_id = $school->id;
        $schoolAdmin->password = Hash::make($request->password);
        $schoolAdmin->save();

        return Response::send(200, null, 'success');
    }

    public function login(Request $request)
    {
        $rules = Validator::make($request->all(), [
            'email' => 'required|email',
            'password'  => 'required|string',
        ]);

        if ($rules->fails()) {
            return Response::send(422, $rules->errors());
        }

        $schoolAdmin = SchoolAdmin::where('email', $request->email)->first();
        
        if (!$schoolAdmin || !Hash::check($request->password, $schoolAdmin->password)) {
            return Response::message('unknown_credentials');
        }

        $token = $schoolAdmin->createToken('school-admin-token')->plainTextToken;

        $response = [
            'user' => $schoolAdmin,
            'token' => $token,
            'role' => 'school_admin'
        ];

        return Response::send(200, $response);
    }

    public function index()
    {
        return Response::send(200, Auth::user());
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        
        return Response::send(200, null, 'logged_out');
    }
}
