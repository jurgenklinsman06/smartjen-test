<?php

namespace App\Http\Controllers\Schools;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\SchoolAdmin;
use App\Responses\Response;
use App\Services\SchoolService;

class SchoolController extends Controller
{
    public function students(Request $request, SchoolService $schoolService)
    {
        $students = $schoolService->students($request, Auth::user()->school_id);

        return Response::send(200, $students);
    }

    public function teachers(Request $request, SchoolService $schoolService)
    {
        $teachers = $schoolService->teachers($request, Auth::user()->school_id);

        return Response::send(200, $teachers);
    }

    public function admins(Request $request)
    {
        $admins = SchoolAdmin::where('school_id', Auth::user()->school_id)->paginate();

        return Response::send(200, $admins);
    }

    public function deleteAdmin(Request $request, $adminId)
    {
        if ($adminId == Auth::user()->id) {
            return Response::message('not_allowed');
        }

        $admin = SchoolAdmin::where('id', $adminId)
            ->where('school_id', Auth::user()->school_id)->first();

        if (null == $admin) {
            return Response::message('unknown_resources');
        }

        $admin->delete();

        return Response::send(200, 'success_delete');
    }
}
