<?php

namespace App\Http\Controllers;

use App\Responses\Response;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();

        return Response::send(200, null, 'logged_out');
    }
}
