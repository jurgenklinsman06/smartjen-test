<?php

namespace App\Services;

use App\Models\SchoolTeacher;
use App\Models\Student;
use App\Models\Teacher;

class SchoolService
{
    public function students($request, $schoolId)
    {
        $students = Student::where('school_id', $schoolId)->paginate();

        return $students;
    }

    public function teachers($request, $schoolId)
    {
        $schoolTeachers = SchoolTeacher::select('teacher_id')
                        ->where('school_id', $schoolId)
                        ->get()->pluck('teacher_id');
        
        $teachers = Teacher::whereIn('id', $schoolTeachers)->paginate();

        return $teachers;
    }
}
