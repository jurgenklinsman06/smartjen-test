import Api from "./Api";
import Csrf from "./Csrf";

export default {
    async register(form) {
        await Csrf.getCookie();

        return Api.post("/school/register", form);
    },

    async registerTeacher(form) {
        await Csrf.getCookie();

        return Api.post("/teacher/register", form);
    },

    async registerStudent(form) {
        await Csrf.getCookie();

        return Api.post("/student/register", form);
    },

    async login(form) {
        await Csrf.getCookie();

        return Api.post("/school/login", form);
    },

    async loginTeacher(form) {
        await Csrf.getCookie();

        return Api.post("/teacher/login", form);
    },

    async loginStudent(form) {
        await Csrf.getCookie();

        return Api.post("/student/login", form);
    },

    async students() {
        await Csrf.getCookie();

        return Api.get("/student/list");
    },

    async teachers() {
        await Csrf.getCookie();

        return Api.get("/student/teachers");
    },
    async schoolTeachers() {
        await Csrf.getCookie();

        return Api.get("/school/teachers");
    },

    async admins() {
        await Csrf.getCookie();

        return Api.get("/school/admins");
    },

    async adminDelete(adminId) {
        await Csrf.getCookie();

        return Api.delete(`/school/admin/${adminId}`);
    },

    async schoolStudents() {
        await Csrf.getCookie();

        return Api.get("/school/students");
    },

    async teacherList(schoolId) {
        await Csrf.getCookie();

        return Api.get(`/teacher/${schoolId}/list`);
    },

    async studentList(schoolId) {
        await Csrf.getCookie();

        return Api.get(`/teacher/${schoolId}/students`);
    },

    async logout() {
        await Csrf.getCookie();

        return Api.post("/logout");
    },

    auth() {
        return Api.get("/school/me");
    }
};
