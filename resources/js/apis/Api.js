import axios from "axios";
import store from "../store";
let Api = axios.create({
    baseURL: "http://157.245.50.20/api"
});

const token = store.getters.getAuthFormGetters
    ? store.getters.getAuthFormGetters.token
    : null;
Api.defaults.withCredentials = true;

if (token) {
    Api.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    Api.defaults.headers.common.Accept = "application/json";
}

export default Api;
