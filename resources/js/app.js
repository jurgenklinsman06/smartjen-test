/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

import router from "./routes";
import App from "./App.vue";
import store from "./store";

Vue.config.productionTip = false;

const app = new Vue({
    el: "#app",
    router,
    store,
    render: h => h(App)
});
