import Vue from "vue";
import VueRouter from "vue-router";
import Cookies from "js-cookie";
import { is } from "@babel/types";

Vue.use(VueRouter);

const routes = [
    {
        path: "*",
        name: "NotFound",
        meta: { layout: "Auth" },
        component: () => import("./components/NotFound.vue")
    },
    {
        path: "/",
        name: "Home",
        meta: { layout: "Auth" },
        component: () => import("./components/Home.vue")
    },
    {
        path: "/about",
        name: "About",
        component: () => import("./views/Admin.vue")
    },
    {
        path: "/dashboard",
        name: "Dashboard",
        meta: { requiresAuth: true },
        component: () => import("./views/Home.vue")
    },
    {
        path: "/student/dashboard",
        name: "StudentDashboard",
        meta: { requiresAuth: true },
        component: () => import("./views/StudentHome.vue")
    },
    {
        path: "/teacher/dashboard",
        name: "TeacherDashboard",
        meta: { requiresAuth: true },
        component: () => import("./views/TeacherHome.vue")
    },
    {
        path: "/school/admin",
        name: "Admin",
        meta: { requiresAuth: true },
        component: () => import("./views/Admin.vue")
    },
    {
        path: "/school/student",
        name: "Student",
        meta: { requiresAuth: true },
        component: () => import("./views/Student.vue")
    },
    {
        path: "/school/teacher",
        name: "Teacher",
        meta: { requiresAuth: true },
        component: () => import("./views/Teacher.vue")
    },
    {
        path: "/login",
        name: "Login",
        meta: { layout: "Auth" },
        component: () => import("./components/Login.vue")
    },
    {
        path: "/student/login",
        name: "StudentLogin",
        meta: { layout: "Auth" },
        component: () => import("./components/StudentLogin.vue")
    },
    {
        path: "/teacher/login",
        name: "TeacherLogin",
        meta: { layout: "Auth" },
        component: () => import("./components/TeacherLogin.vue")
    },
    {
        path: "/student/register",
        name: "RegisterStudent",
        meta: { layout: "Auth" },
        component: () => import("./components/StudentRegister.vue")
    },
    {
        path: "/teacher/register",
        name: "RegisterTeacher",
        meta: { layout: "Auth" },
        component: () => import("./components/TeacherRegister.vue")
    },
    {
        path: "/register",
        name: "Register",
        meta: { layout: "Auth" },
        component: () => import("./components/Register.vue")
    }
];
const router = new VueRouter({
    mode: "history",
    routes
});

router.beforeEach((to, from, next) => {
    const cookie = Cookies.get("SchoolStorage");
    const isAuthenticated = cookie ? JSON.parse(cookie).auth.token : null;
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (to.name !== "Login" && !isAuthenticated) next({ name: "Home" });
        else {
            const sidebar = JSON.parse(cookie).sidebar;
            if (sidebar.find(obj => obj.path === to.path)) {
                next();
            } else {
                next({ name: "NotFound" });
            }
        }
    } else next();
});

export default router;
