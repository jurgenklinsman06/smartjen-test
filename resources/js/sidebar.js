export const listSideBar = [
    {
        path: "/dashboard",
        title: "Dashboard",
        role: "school_admin",
        icon: "fas fa-home"
    },
    {
        path: "/dashboard",
        title: "Dashboard",
        role: "teacher",
        icon: "fas fa-home"
    },
    {
        path: "/student/dashboard",
        title: "Dashboard",
        role: "student",
        icon: "fas fa-home"
    },
    {
        path: "/teacher/dashboard",
        title: "Dashboard",
        role: "teacher",
        icon: "fas fa-home"
    },
    {
        path: "/school/admin",
        title: "Admin",
        role: "school_admin",
        icon: "fas fa-users-cog"
    },
    {
        path: "/school/teacher",
        title: "Teacher",
        role: "school_admin",
        icon: "fas fa-chalkboard-teacher"
    },
    {
        path: "/school/student",
        title: "Student",
        role: "school_admin",
        icon: "fas fa-user-graduate"
    }
];
