import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import Cookies from "js-cookie";
import { listSideBar } from "../sidebar";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        auth: null,
        sidebar: []
    },
    plugins: [
        createPersistedState({
            key: "SchoolStorage",
            storage: {
                getItem: key => Cookies.get(key),
                setItem: (key, value) =>
                    Cookies.set(key, value, { expires: 3, secure: true }),
                removeItem: key => Cookies.remove(key)
            }
        })
    ],
    getters: {
        getAuthFormGetters(state) {
            return state.auth;
        }
    },
    actions: {
        getAuthData({ commit, dispatch }, data) {
            commit("SET_AUTH", data);
            dispatch("getSidebarData", data.role);
        },
        getSidebarData({ commit }, role) {
            const sidebar = listSideBar.filter(obj => obj.role === role);
            commit("SET_SIDEBAR", sidebar);
        },
        logout({ commit }) {
            Cookies.remove("SchoolStorage");
            location.reload();
        }
    },

    mutations: {
        SET_AUTH: (state, data) => {
            state.auth = data;
        },
        SET_SIDEBAR: (state, data) => {
            state.sidebar = data;
        }
    }
});
