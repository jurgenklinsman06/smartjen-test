<?php

use App\Http\Controllers\AuthController as ControllersAuthController;
use App\Http\Controllers\Schools\AuthController;
use App\Http\Controllers\Schools\SchoolController;
use App\Http\Controllers\Students\StudentAuthController;
use App\Http\Controllers\Students\StudentController;
use App\Http\Controllers\Teachers\TeacherAuthController;
use App\Http\Controllers\Teachers\TeacherController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'school'], function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login'])->name('login');

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/me', [AuthController::class, 'index']);
        Route::get('/admins', [SchoolController::class, 'admins']);
        Route::get('/students', [SchoolController::class, 'students']);
        Route::get('/teachers', [SchoolController::class, 'teachers']);
        Route::post('/logout', [AuthController::class, 'logout']);
        
        Route::delete('/admin/{adminId}', [SchoolController::class, 'deleteAdmin']);
    });
});

Route::group(['prefix' => 'student'], function () {
    Route::post('/register', [StudentAuthController::class, 'register']);
    Route::post('/login', [StudentAuthController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/me', [StudentAuthController::class, 'index']);
        Route::get('/list', [StudentController::class, 'students']);
        Route::get('/teachers', [StudentController::class, 'teachers']);
        Route::post('/logout', [StudentAuthController::class, 'logout'])->name('logout');
    });
});

Route::group(['prefix' => 'teacher'], function () {
    Route::post('/register', [TeacherAuthController::class, 'register']);
    Route::post('/login', [TeacherAuthController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/me', [TeacherAuthController::class, 'index']);
        Route::get('/{schoolId}/list', [TeacherController::class, 'teachers']);
        Route::get('/{schoolId}/students', [TeacherController::class, 'students']);

        Route::post('/logout', [TeacherAuthController::class, 'logout'])->name('logout');
    });
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('/logout', [ControllersAuthController::class, 'logout'])->name('logout');
});
