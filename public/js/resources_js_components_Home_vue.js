"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_Home_vue"],{

/***/ "./resources/js/components/Home.vue":
/*!******************************************!*\
  !*** ./resources/js/components/Home.vue ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Home_vue_vue_type_template_id_f2b6376c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=f2b6376c& */ "./resources/js/components/Home.vue?vue&type=template&id=f2b6376c&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__.default)(
  script,
  _Home_vue_vue_type_template_id_f2b6376c___WEBPACK_IMPORTED_MODULE_0__.render,
  _Home_vue_vue_type_template_id_f2b6376c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Home.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Home.vue?vue&type=template&id=f2b6376c&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/Home.vue?vue&type=template&id=f2b6376c& ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_f2b6376c___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_f2b6376c___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_f2b6376c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Home.vue?vue&type=template&id=f2b6376c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home.vue?vue&type=template&id=f2b6376c&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home.vue?vue&type=template&id=f2b6376c&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Home.vue?vue&type=template&id=f2b6376c& ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", {}, [
    _vm._m(0),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "card-deck mb-3 text-center" }, [
        _c("div", { staticClass: "card mb-4 shadow-sm" }, [
          _vm._m(2),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("h1", { staticClass: "card-title pricing-card-title" }, [
                _vm._v(
                  "\n                        Start as a Student\n                    "
                )
              ]),
              _vm._v(" "),
              _c("router-link", { attrs: { to: "student/register" } }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-lg btn-block btn-outline-primary",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v(
                      "\n                            Sign up\n                        "
                    )
                  ]
                )
              ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card mb-4 shadow-sm" }, [
          _vm._m(3),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body" },
            [
              _c("h1", { staticClass: "card-title pricing-card-title" }, [
                _vm._v(
                  "\n                        Start as a Teacher\n                    "
                )
              ]),
              _vm._v(" "),
              _c("router-link", { attrs: { to: "teacher/register" } }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-lg btn-block btn-outline-primary",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v(
                      "\n                        Sign up\n                    "
                    )
                  ]
                )
              ])
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "card mb-4 shadow-sm" }, [
          _vm._m(4),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card-body text-center" },
            [
              _c("h1", { staticClass: "card-title pricing-card-title" }, [
                _vm._v(
                  "\n                        Register your School\n                    "
                )
              ]),
              _vm._v(" "),
              _c("router-link", { attrs: { to: "register" } }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-lg btn-block btn-outline-primary",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v(
                      "\n                        Sign up\n                    "
                    )
                  ]
                )
              ])
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _vm._m(5)
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm"
      },
      [
        _c("h5", { staticClass: "my-0 mr-md-auto font-weight-normal" }, [
          _vm._v("SmartScool")
        ]),
        _vm._v(" "),
        _c("nav", { staticClass: "my-2 my-md-0 mr-md-3" }, [
          _c("a", { staticClass: "p-2 text-dark", attrs: { href: "#" } }, [
            _vm._v("Features")
          ]),
          _vm._v(" "),
          _c("a", { staticClass: "p-2 text-dark", attrs: { href: "#" } }, [
            _vm._v("Support")
          ]),
          _vm._v(" "),
          _c("a", { staticClass: "p-2 text-dark", attrs: { href: "#" } }, [
            _vm._v("Pricing")
          ])
        ]),
        _vm._v(" "),
        _c(
          "a",
          { staticClass: "btn btn-outline-primary", attrs: { href: "#" } },
          [_vm._v("Sign up")]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass:
          "pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center container"
      },
      [
        _c("h1", { staticClass: "display-4" }, [_vm._v("SmartScool")]),
        _vm._v(" "),
        _c("p", { staticClass: "lead" }, [
          _vm._v(
            "\n            “Education is the most powerful weapon you can use to change the\n            world.” "
          ),
          _c("b", [_vm._v("Nelson Mandela")])
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h4", { staticClass: "my-0 font-weight-normal" }, [_vm._v("Student")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h4", { staticClass: "my-0 font-weight-normal" }, [_vm._v("Teacher")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h4", { staticClass: "my-0 font-weight-normal" }, [_vm._v("School")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", { staticClass: "pt-4 my-md-5 pt-md-5 border-top" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-12 col-md" }, [
          _c("small", { staticClass: "d-block mb-3 text-muted" }, [
            _vm._v("© 2021")
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ })

}]);