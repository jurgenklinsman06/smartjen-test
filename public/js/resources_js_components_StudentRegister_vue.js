"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_StudentRegister_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/StudentRegister.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/StudentRegister.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _apis_Endpoint__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../apis/Endpoint */ "./resources/js/apis/Endpoint.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      form: {
        school_id: "",
        name: "",
        email: "",
        password: "",
        password_confirmation: ""
      },
      errorTnc: false,
      tnc: false,
      errors: {
        email: "",
        password: "",
        school_id: "",
        name: "",
        password_confirmation: ""
      }
    };
  },
  watch: {
    "form.school_name": function formSchool_name(newVal) {
      if (newVal) {
        this.errors.school_name = "";
      }
    },
    "form.name": function formName(newVal) {
      if (newVal) {
        this.errors.name = "";
      }
    },
    "form.email": function formEmail(newVal) {
      if (newVal) {
        this.errors.email = "";
      }
    },
    "form.password": function formPassword(newVal) {
      if (newVal) {
        this.errors.password = "";
      }
    },
    "form.password_confirmation": function formPassword_confirmation(newVal) {
      if (newVal) {
        this.errors.password_confirmation = "";
      }
    },
    tnc: function tnc() {
      if (this.tnc) {
        this.errorTnc = false;
      }
    }
  },
  methods: {
    register: function register() {
      var _this = this;

      if (!this.tnc) {
        this.errorTnc = true;
        return;
      }

      _apis_Endpoint__WEBPACK_IMPORTED_MODULE_0__.default.registerStudent(this.form).then(function () {
        _this.$router.push({
          name: "Login"
        });
      })["catch"](function (error) {
        if (error.response.status == 422) {
          var err = error.response.data.data;
          var errordata = _this.errors;

          var _loop = function _loop(x) {
            errordata[x] = err.find(function (e) {
              return e.field === x;
            });
          };

          for (var x in errordata) {
            _loop(x);
          }

          _this.errors = errordata;
        }
      });
    }
  }
});

/***/ }),

/***/ "./resources/js/components/StudentRegister.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/StudentRegister.vue ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _StudentRegister_vue_vue_type_template_id_57f38763___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StudentRegister.vue?vue&type=template&id=57f38763& */ "./resources/js/components/StudentRegister.vue?vue&type=template&id=57f38763&");
/* harmony import */ var _StudentRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StudentRegister.vue?vue&type=script&lang=js& */ "./resources/js/components/StudentRegister.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__.default)(
  _StudentRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__.default,
  _StudentRegister_vue_vue_type_template_id_57f38763___WEBPACK_IMPORTED_MODULE_0__.render,
  _StudentRegister_vue_vue_type_template_id_57f38763___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/StudentRegister.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/StudentRegister.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/StudentRegister.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StudentRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./StudentRegister.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/StudentRegister.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StudentRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__.default); 

/***/ }),

/***/ "./resources/js/components/StudentRegister.vue?vue&type=template&id=57f38763&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/StudentRegister.vue?vue&type=template&id=57f38763& ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StudentRegister_vue_vue_type_template_id_57f38763___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StudentRegister_vue_vue_type_template_id_57f38763___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StudentRegister_vue_vue_type_template_id_57f38763___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./StudentRegister.vue?vue&type=template&id=57f38763& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/StudentRegister.vue?vue&type=template&id=57f38763&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/StudentRegister.vue?vue&type=template&id=57f38763&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/StudentRegister.vue?vue&type=template&id=57f38763& ***!
  \***************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "hold-transition register-page" }, [
    _vm.errorTnc
      ? _c(
          "div",
          { staticClass: "alert alert-danger", attrs: { role: "alert" } },
          [_vm._v("\n        Accept Terms and Condition\n    ")]
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "register-box" }, [
      _c("div", { staticClass: "card card-outline card-primary" }, [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "card-body" },
          [
            _c("p", { staticClass: "login-box-msg" }, [
              _vm._v("Register as a student")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "input-group mb-3" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.school_id,
                    expression: "form.school_id"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "text",
                  id: "school_id",
                  placeholder: "School ID"
                },
                domProps: { value: _vm.form.school_id },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "school_id", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _vm.errors.school_id
                ? _c("span", { staticClass: "text-danger w-100" }, [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.errors.school_id.error) +
                        "\n                    "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "input-group mb-3" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.name,
                    expression: "form.name"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "text", id: "name", placeholder: "Full name" },
                domProps: { value: _vm.form.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "name", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(2),
              _vm._v(" "),
              _vm.errors.name
                ? _c("span", { staticClass: "text-danger w-100" }, [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.errors.name.error) +
                        "\n                    "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "input-group mb-3" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.email,
                    expression: "form.email"
                  }
                ],
                staticClass: "form-control",
                attrs: { type: "email", id: "email", placeholder: "Email" },
                domProps: { value: _vm.form.email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "email", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(3),
              _vm._v(" "),
              _vm.errors.email
                ? _c("span", { staticClass: "text-danger w-100" }, [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.errors.email.error) +
                        "\n                    "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "input-group mb-3" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.password,
                    expression: "form.password"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "password",
                  id: "password",
                  placeholder: "Password"
                },
                domProps: { value: _vm.form.password },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "password", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(4),
              _vm._v(" "),
              _vm.errors.password
                ? _c("span", { staticClass: "text-danger w-100" }, [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.errors.password.error) +
                        "\n                    "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "input-group mb-3" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.password_confirmation,
                    expression: "form.password_confirmation"
                  }
                ],
                staticClass: "form-control",
                attrs: {
                  type: "password",
                  id: "password_confirmation",
                  placeholder: "Retype password"
                },
                domProps: { value: _vm.form.password_confirmation },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(
                      _vm.form,
                      "password_confirmation",
                      $event.target.value
                    )
                  }
                }
              }),
              _vm._v(" "),
              _vm._m(5),
              _vm._v(" "),
              _vm.errors.password_confirmation
                ? _c("span", { staticClass: "text-danger w-100" }, [
                    _vm._v(
                      "\n                        " +
                        _vm._s(_vm.errors.password_confirmation.error) +
                        "\n                    "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-8" }, [
                _c("div", { staticClass: "icheck-primary" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.tnc,
                        expression: "tnc"
                      }
                    ],
                    attrs: {
                      type: "checkbox",
                      id: "agreeTerms",
                      name: "terms",
                      value: "agree"
                    },
                    domProps: {
                      checked: Array.isArray(_vm.tnc)
                        ? _vm._i(_vm.tnc, "agree") > -1
                        : _vm.tnc
                    },
                    on: {
                      change: function($event) {
                        var $$a = _vm.tnc,
                          $$el = $event.target,
                          $$c = $$el.checked ? true : false
                        if (Array.isArray($$a)) {
                          var $$v = "agree",
                            $$i = _vm._i($$a, $$v)
                          if ($$el.checked) {
                            $$i < 0 && (_vm.tnc = $$a.concat([$$v]))
                          } else {
                            $$i > -1 &&
                              (_vm.tnc = $$a
                                .slice(0, $$i)
                                .concat($$a.slice($$i + 1)))
                          }
                        } else {
                          _vm.tnc = $$c
                        }
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._m(6)
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-4" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-block",
                    attrs: { type: "submit" },
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.register.apply(null, arguments)
                      }
                    }
                  },
                  [
                    _vm._v(
                      "\n                            Register\n                        "
                    )
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "router-link",
              { staticClass: "text-center", attrs: { to: "/student/login" } },
              [_vm._v("I already have an account")]
            )
          ],
          1
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header text-center" }, [
      _c("span", [_c("b", [_vm._v("Smart")]), _vm._v("Jen ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [
        _c("span", { staticClass: "fas fa-school" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [
        _c("span", { staticClass: "fas fa-user" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [
        _c("span", { staticClass: "fas fa-envelope" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [
        _c("span", { staticClass: "fas fa-lock" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [
        _c("span", { staticClass: "fas fa-lock" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", { attrs: { for: "agreeTerms" } }, [
      _vm._v("\n                                I agree to the "),
      _c("a", { attrs: { href: "#" } }, [_vm._v("terms")])
    ])
  }
]
render._withStripped = true



/***/ })

}]);